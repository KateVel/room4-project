import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Track } from '../models/track.model';
import { Tracks } from '../models/tracks.model';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.css']
})
export class MainPageComponent implements OnInit {
  tracksList: Observable<Track[]>;

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.tracksList = this.http.get("", {params: {method: 'chart.gettoptracks' }})
      .pipe(
        map((tracks: Tracks) => {
          return tracks.tracks.track;
        })
      )
  }
}
