import { Track } from './track.model';

export interface Tracks {
 tracks: {
   '@attr': any;
   track: Track[];
 }

}