export interface Track {
    artist: {
        mbid: string;
        name: string;
        url: string;
    };
    duratiton: string;
    image: Array<{
        '#text': string;
        size: string;
    }>;
    listeners: string;
    mbid: string;
    name: string;
    playcoun: string;
    streamable: any;
    url: string;
}