import { HttpHandler, HttpInterceptor, HttpParams, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class RequestIntercetorService implements HttpInterceptor {

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    const API_KEY = 'd5f79ecd05370e311c42d34bb1d7fcf5';
    const BASIC_URL = 'http://ws.audioscrobbler.com/2.0/';
    let mainParams = req.params;
    mainParams = mainParams.append('api_key', API_KEY);
    mainParams = mainParams.append('format', 'json');

    const modifiedRequest = req.clone({
      url: `${BASIC_URL}`, 
      params: mainParams,
    });

    return next.handle(modifiedRequest);
  }
}
