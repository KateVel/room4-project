import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Observable, pipe } from 'rxjs';
import { map, subscribeOn } from 'rxjs/operators';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  signupForm: FormGroup;

  tracksList: Observable<any>;

  constructor(private http: HttpClient) { }

  ngOnInit(): void {

    this.signupForm = new FormGroup({
      trackName: new FormControl(null)
    })
  }

  onSubmit(): void {
    this.tracksList = this.http.get("", { params: { method: 'track.search', track: this.signupForm.value.trackName } })
      .pipe(
        map((data: any) => {
          return data.results.trackmatches.track;
        })
      );
    this.signupForm.reset();
  }
}
